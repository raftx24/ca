
module sign_ext (output[31:0] to,input[15:0] from);
	assign to[15:0] = from[15:0];
	assign to[31:16] = (from[15] == 0) ? 16'b0 : 16'b1111111111111111;
endmodule
