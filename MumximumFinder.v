
module instruction_memory (
	output reg[31:0] instruction,input[31:0] instruction_address
);
	// Temp Comment
	parameter FUNC_AND = 6'b100100;
	parameter FUNC_OR = 6'b100101;
	parameter FUNC_ADD = 6'b100000;
	parameter FUNC_SUB = 6'b100010;
	parameter FUNC_XOR = 6'b100110;
	parameter FUNC_MULT = 6'b011000;
	parameter FUNC_MFLO = 6'b010010;
	parameter FUNC_MFHI = 6'b010000;
	parameter FUNC_JR = 6'b001000;
	// Opcode Field (From Instruction)
	parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	parameter OP_J = 6'b000010;
	parameter OP_JAL = 6'b000011;
	// Extension
	parameter SIGN_EXT = 2'b00;
	parameter ZERO_EXT = 2'b01;
	parameter LUI_EXT = 2'b10;
	// PC Selection
	parameter PC_DEFAULT = 2'b00;
	parameter PC_JR = 2'b01;
	parameter PC_BRANCH = 2'b10;
	parameter PC_JUMP = 2'b11;
	parameter REG_R0=5'b00000;
	parameter REG_R1=5'b00001;
	parameter REG_R2=5'b00010;
	parameter REG_R3=5'b00011;
	parameter REG_R4=5'b00100;
	parameter REG_R5=5'b00101;
	parameter REG_R6=5'b00110;
	parameter REG_R7=5'b00111;
	parameter REG_R8=5'b01000;
	parameter REG_R9=5'b01001;
	parameter REG_R10=5'b01010;
	parameter REG_R11=5'b01011;
	parameter REG_R12=5'b01100;
	parameter REG_R13=5'b01101;
	parameter REG_R14=5'b01110;
	parameter REG_R15=5'b01111;
	parameter REG_R16=5'b10000;
	parameter REG_R17=5'b10001;
	parameter REG_R18=5'b10010;
	parameter REG_R19=5'b10011;
	parameter REG_R20=5'b10100;
	parameter REG_R21=5'b10101;
	parameter REG_R22=5'b10110;
	parameter REG_R23=5'b10111;
	parameter REG_R24=5'b11000;
	parameter REG_R25=5'b11001;
	parameter REG_R26=5'b11010;
	parameter REG_R27=5'b11011;
	parameter REG_R28=5'b11100;
	parameter REG_R29=5'b11101;
	parameter REG_R30=5'b11110;
	parameter REG_R31=5'b11111;
	reg START=32'd1000;
	/**
	R1 <- i
	R2 <- 1080
	R8 <- 10000000000000
	R3 <- A[i]
	R3 <- R2-max
	R3 <- R3 & R1
	R6 <- max
	R7 <- index of max
	R10 <- temp
	**/
	always @(instruction_address) begin
		case(instruction_address)

			0: instruction <= {OP_ADDI,REG_R0,REG_R10,16'd1000};
			4: instruction <= {OP_SW,REG_R0,REG_R10,16'd1000}; // populate array with data
			8: instruction <= {OP_ADDI,REG_R10,REG_R10,16'd1000};
			12: instruction <= {OP_SW,REG_R0,REG_R0,16'd1040};// populate array with data
			16: instruction <= {OP_SW,REG_R0,REG_R10,16'd1020};// populate array with data


			20: instruction <= {OP_LUI,5'b00000,REG_R8,16'd1000000000000000};
			24: instruction <= {OP_ADDI,REG_R0,REG_R1,16'd1000}; // array pointer , and array start 1000
			28: instruction <= {OP_LW,REG_R1,REG_R6,16'd0}; // init with A[0]
			32: instruction <= {OP_ADDI,REG_R0,REG_R7,16'd1000}; // init with first index 1000
			36: instruction <= {OP_ADDI,REG_R0,REG_R2,16'd1080}; // end of array

			40: instruction <= {OP_BEQ,REG_R1,REG_R2,16'd8};
				44: instruction <= {OP_LW,REG_R1,REG_R3,16'd0};
				48: instruction <= {OP_R_TYPE,REG_R3,REG_R6,REG_R3,5'd0,FUNC_SUB};
				52: instruction <= {OP_R_TYPE,REG_R3,REG_R8,REG_R3,5'd0,FUNC_AND};
				56: instruction <= {OP_BEQ,REG_R8,REG_R3,16'd2};
					60: instruction <= {OP_LW,REG_R1,REG_R6,16'd0};	
					64: instruction <= {OP_R_TYPE,REG_R0,REG_R1,REG_R7,5'd0,FUNC_ADD};
			68: instruction <= {OP_ADDI,REG_R1,REG_R1,16'd4};
			72: instruction <= {OP_J,26'd10};

			76: instruction <= {OP_SW,REG_R0,REG_R6,16'd2000};
			80: instruction <= {OP_SW,REG_R0,REG_R7,16'd2004};
			84: instruction <= {OP_LW,REG_R0,REG_R5,16'd2000};
			88: instruction <= {OP_ADDI,REG_R5,REG_R5,16'd0};
			92: instruction <= {OP_LW,REG_R0,REG_R5,16'd2004};
			96: instruction <= {OP_ADDI,REG_R5,REG_R5,16'd0};
		endcase
	end
endmodule
