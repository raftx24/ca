
module mips(input clk,rst);
	wire[5:0] opcode;
	wire[5:0] func;
	wire reg_dst,reg_write,alu_src,is_jal,mem_read,mem_write,mem_to_reg,zero;
	wire[1:0] sel_extension;
	wire[1:0] pc_select;
	wire[2:0] alu_operation;
	wire[31:0] temp;
	datapath DATAPATH_(temp,opcode,func,zero,
					reg_dst,reg_write,alu_src,is_jal,mem_read,mem_write,mem_to_reg,
					sel_extension,pc_select,alu_operation,clk,rst);
	controller CONTROLLER_(reg_dst,reg_write,is_lui,alu_src,is_jal,mem_read,mem_write,mem_to_reg,
					sel_extension,pc_select,opcode,func,zero,rst,clk);
	alu_controller ALU_CONTROLLER_(alu_operation,opcode,func);
endmodule