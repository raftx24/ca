
module mux4 #(parameter WIDTH = 32) 
	(output [WIDTH-1:0] out,input[WIDTH-1:0] in0,in1,in2,in3,input [1:0]sel,input enable);
	assign out = (enable == 1) ? (sel == 0) ? in0 :
				 (sel == 1) ? in1 :
				 (sel == 2) ? in2 :
				 (sel == 3) ? in3 : 'z : 'z;
	
endmodule
