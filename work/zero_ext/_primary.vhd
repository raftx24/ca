library verilog;
use verilog.vl_types.all;
entity zero_ext is
    generic(
        \TO\            : integer := 32;
        \FROM\          : integer := 16
    );
    port(
        \to\            : out    vl_logic_vector;
        from            : in     vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of \TO\ : constant is 1;
    attribute mti_svvh_generic_type of \FROM\ : constant is 1;
end zero_ext;
