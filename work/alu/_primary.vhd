library verilog;
use verilog.vl_types.all;
entity alu is
    port(
        \out\           : out    vl_logic_vector(31 downto 0);
        zero            : out    vl_logic;
        clk             : in     vl_logic;
        alu_operation   : in     vl_logic_vector(2 downto 0);
        in1             : in     vl_logic_vector(31 downto 0);
        in2             : in     vl_logic_vector(31 downto 0)
    );
end alu;
