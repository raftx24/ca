library verilog;
use verilog.vl_types.all;
entity \register\ is
    generic(
        SIZE            : integer := 8
    );
    port(
        q               : out    vl_logic_vector;
        d               : in     vl_logic_vector;
        ld              : in     vl_logic;
        rst             : in     vl_logic;
        clk             : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of SIZE : constant is 1;
end \register\;
