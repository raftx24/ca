library verilog;
use verilog.vl_types.all;
entity decoder is
    generic(
        SIZE            : integer := 4
    );
    port(
        \out\           : out    vl_logic_vector;
        \in\            : in     vl_logic_vector;
        enable          : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of SIZE : constant is 1;
end decoder;
