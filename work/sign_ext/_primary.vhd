library verilog;
use verilog.vl_types.all;
entity sign_ext is
    port(
        \to\            : out    vl_logic_vector(31 downto 0);
        from            : in     vl_logic_vector(15 downto 0)
    );
end sign_ext;
