library verilog;
use verilog.vl_types.all;
entity mux is
    generic(
        SIZE            : integer := 32;
        WIDTH           : integer := 32;
        SEL_SIZE        : integer := 5
    );
    port(
        \out\           : out    vl_logic_vector;
        \in\            : in     vl_logic_vector;
        sel             : in     vl_logic_vector;
        enable          : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of SIZE : constant is 1;
    attribute mti_svvh_generic_type of WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SEL_SIZE : constant is 1;
end mux;
