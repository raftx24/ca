library verilog;
use verilog.vl_types.all;
entity datapath is
    generic(
        OP_R_TYPE       : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        OP_ADDI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi0, Hi0, Hi0);
        OP_ANDI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi0, Hi0);
        OP_ORI          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi0, Hi1);
        OP_XORI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi1, Hi0);
        OP_LUI          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi1, Hi1);
        OP_BEQ          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi1, Hi0, Hi0);
        OP_BNE          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi1, Hi0, Hi1);
        OP_LW           : vl_logic_vector(0 to 5) := (Hi1, Hi0, Hi0, Hi0, Hi1, Hi1);
        OP_SW           : vl_logic_vector(0 to 5) := (Hi1, Hi0, Hi1, Hi0, Hi1, Hi1);
        OP_J            : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi1, Hi0);
        OP_JAL          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi1, Hi1)
    );
    port(
        temp            : out    vl_logic_vector(31 downto 0);
        opcode          : out    vl_logic_vector(5 downto 0);
        func            : out    vl_logic_vector(5 downto 0);
        equal           : out    vl_logic;
        reg_dst         : in     vl_logic;
        reg_write       : in     vl_logic;
        alu_src         : in     vl_logic;
        is_jal          : in     vl_logic;
        mem_read        : in     vl_logic;
        mem_write       : in     vl_logic;
        mem_to_reg      : in     vl_logic;
        sel_extension   : in     vl_logic_vector(1 downto 0);
        pc_select       : in     vl_logic_vector(1 downto 0);
        alu_operation   : in     vl_logic_vector(2 downto 0);
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of OP_R_TYPE : constant is 1;
    attribute mti_svvh_generic_type of OP_ADDI : constant is 1;
    attribute mti_svvh_generic_type of OP_ANDI : constant is 1;
    attribute mti_svvh_generic_type of OP_ORI : constant is 1;
    attribute mti_svvh_generic_type of OP_XORI : constant is 1;
    attribute mti_svvh_generic_type of OP_LUI : constant is 1;
    attribute mti_svvh_generic_type of OP_BEQ : constant is 1;
    attribute mti_svvh_generic_type of OP_BNE : constant is 1;
    attribute mti_svvh_generic_type of OP_LW : constant is 1;
    attribute mti_svvh_generic_type of OP_SW : constant is 1;
    attribute mti_svvh_generic_type of OP_J : constant is 1;
    attribute mti_svvh_generic_type of OP_JAL : constant is 1;
end datapath;
