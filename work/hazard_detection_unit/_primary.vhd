library verilog;
use verilog.vl_types.all;
entity hazard_detection_unit is
    generic(
        OP_R_TYPE       : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        OP_ADDI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi0, Hi0, Hi0);
        OP_ANDI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi0, Hi0);
        OP_ORI          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi0, Hi1);
        OP_XORI         : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi1, Hi0);
        OP_LUI          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi1, Hi1, Hi1, Hi1);
        OP_BEQ          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi1, Hi0, Hi0);
        OP_BNE          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi1, Hi0, Hi1);
        OP_LW           : vl_logic_vector(0 to 5) := (Hi1, Hi0, Hi0, Hi0, Hi1, Hi1);
        OP_SW           : vl_logic_vector(0 to 5) := (Hi1, Hi0, Hi1, Hi0, Hi1, Hi1);
        OP_J            : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi1, Hi0);
        OP_JAL          : vl_logic_vector(0 to 5) := (Hi0, Hi0, Hi0, Hi0, Hi1, Hi1)
    );
    port(
        IF_FLUSH        : out    vl_logic;
        pc_write_enable : out    vl_logic;
        is_equal        : in     vl_logic;
        opcode          : in     vl_logic_vector(5 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of OP_R_TYPE : constant is 1;
    attribute mti_svvh_generic_type of OP_ADDI : constant is 1;
    attribute mti_svvh_generic_type of OP_ANDI : constant is 1;
    attribute mti_svvh_generic_type of OP_ORI : constant is 1;
    attribute mti_svvh_generic_type of OP_XORI : constant is 1;
    attribute mti_svvh_generic_type of OP_LUI : constant is 1;
    attribute mti_svvh_generic_type of OP_BEQ : constant is 1;
    attribute mti_svvh_generic_type of OP_BNE : constant is 1;
    attribute mti_svvh_generic_type of OP_LW : constant is 1;
    attribute mti_svvh_generic_type of OP_SW : constant is 1;
    attribute mti_svvh_generic_type of OP_J : constant is 1;
    attribute mti_svvh_generic_type of OP_JAL : constant is 1;
end hazard_detection_unit;
