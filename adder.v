
module adder #(parameter SIZE = 32) (output[SIZE-1:0] out,input[SIZE-1:0] in1,in2);
	assign out = in1 + in2;
endmodule
