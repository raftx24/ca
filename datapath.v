
module datapath(output[31:0] temp,output[5:0] opcode,output[5:0] func,output equal,
					input reg_dst,reg_write,alu_src,is_jal,mem_read,mem_write,mem_to_reg,
					input[1:0] sel_extension,pc_select,input[2:0] alu_operation,input clk,rst);	
	wire[31:0] pc_out;
	wire[31:0] pc_in;
	wire[31:0] pc_plus_4;
	wire[31:0] instruction_out;
	wire[4:0] read_reg1,read_reg2,write_reg;
	wire[31:0] write_data;
	wire[31:0] read_data1,read_data2;
	wire[4:0] reg_dst_out;
	wire[31:0] alu_in1,alu_in2,alu_out;
	wire[31:0] mux_ext_out;
	wire[31:0] sign_ext_out,zero_ext_out,lui_ext;
	wire[31:0] branch_add_out;
	wire[31:0] read_data_mem,write_data_mem,mem_mux_out;
	wire[31:0] jal_adder_out;
	wire zero;
	wire ID_reg_write_out,ID_alu_src_out,ID_mem_read_out,ID_mem_write_out,ID_mem_to_reg_out;
	wire[2:0] ID_alu_opration_out;
	wire EX_reg_write_out,EX_mem_read_out,EX_mem_write_out,EX_mem_to_reg_out;
	wire MEM_reg_write_out,MEM_mem_to_reg_out;
	wire[31:0] read_data_mem_reg,alu_mem_wb_out;
	wire[4:0] rd_mem_wb_out;
	wire[31:0] pc_plus_4_reg;
	wire[31:0] ir_out;
	wire[31:0] a_out,b_out,ext_out;
	wire[4:0] rd_id_ex_out,out,rs_out,rt_out;
	wire[31:0] alu_ex_mem_out,alu_src_b;
	wire[4:0] rd_ex_mem_out;
	wire IF_FLUSH,ID_FLUSH,EX_FLUSH;
	wire[1:0] forward_a,forward_b;
	wire[5:0]ID_opcode_out;
	wire[5:0]EX_opcode_out;
	wire hazard_detection_pc_write_enable;
	parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	parameter OP_J = 6'b000010;
	parameter OP_JAL = 6'b000011;
	// IF Section
		// PC
			register #(32) PC(pc_out,pc_in,hazard_detection_pc_write_enable,rst,clk);
			adder #(32) PC_ADD4(pc_plus_4,pc_out,4);
			mux4 #(32) PC_SEL(pc_in,pc_plus_4,alu_out,branch_add_out,{pc_plus_4[31:28],instruction_out[25:0],2'b00},pc_select,1'b1);
		// Instruction
			instruction_memory INST_MEMORY(instruction_out,pc_out);
			
	// IF/ID Wall
		register #(32) PC_PLUS_4_REG(pc_plus_4_reg,pc_plus_4,1'b1,IF_FLUSH | rst,clk);
		register #(32) IR_REG(ir_out,instruction_out,1'b1,IF_FLUSH | rst,clk);

	// ID Section
		// Reg File
			assign read_reg1 = ir_out[25:21];
			assign read_reg2 = ir_out[20:16];
			mux2 #(5) REG_DST_MUX(reg_dst_out,ir_out[20:16],ir_out[15:11],reg_dst,1'b1);
			mux2 #(5) JAL_REG_DST_MUX(write_reg,reg_dst_out,5'b11111,is_jal,1'b1);
			register_file REG_FILE(read_data1,read_data2,read_reg1,read_reg2,rd_mem_wb_out,write_data,MEM_reg_write_out,clk,rst);
		// Branch
		assign equal = read_data1 == read_data2;
			adder #(32) BRANCH_ADDER(branch_add_out,pc_plus_4_reg,mux_ext_out << 2);
		// Jump
			
		// Extension
			sign_ext SIGN_EXT(sign_ext_out,ir_out[15:0]);
			zero_ext #(32,16) ZERO_EXT(zero_ext_out,ir_out[15:0]);
			assign lui_ext = {ir_out[15:0],16'b0};
			mux4 #(32) SEL_EXT_MUX(mux_ext_out,sign_ext_out,zero_ext_out,lui_ext,32'b0,sel_extension,1'b1);

	// ID/EX Wall
		// wire[31:0] forward_reg_a,forward_reg_b;  
		// mux2 #(32) MUX_IF_WRITE_AND_READ_SAME_REG_FOR_A(forward_reg_a,read_data1,write_data,(rd_mem_wb_out == read_reg1),1'b1);
		// mux2 #(32) MUX_IF_WRITE_AND_READ_SAME_REG_FOR_B(forward_reg_b,read_data2,write_data,(rd_mem_wb_out == read_reg2),1'b1);
		register #(32) A(a_out,read_data1,1'b1,rst,clk);
		register #(32) B(b_out,read_data2,1'b1,rst,clk);
		register #(5) RS(rs_out,read_reg1,1'b1,rst,clk);
		register #(5) RT(rt_out,read_reg2,1'b1,rst,clk);
		register #(5) RD_ID_EX(rd_id_ex_out,write_reg,1'b1,rst,clk);
		register #(32) EXT(ext_out,mux_ext_out,1'b1,rst,clk);
		// register #(32) PC_PLUS_4_ID_EX_REG();
	// EX Section
		// ALU

			wire[31:0] forward_a_mux_out,forward_b_mux_out,aluSrcB;
			assign alu_in1 = a_out;
			//mux2 #(32) ALU_SRC(alu_in2,b_out,ext_out,ID_alu_src_out,1'b1);
			//rd_ex_mem_out,rd_mem_wb_out, rs_out,rt_out,EX_reg_write_out,MEM_reg_write_out
			mux4 #(32) FORWARD_A_MUX(forward_a_mux_out,a_out,write_data,alu_ex_mem_out,32'bz,forward_a,1'b1);
			mux4 #(32) FORWARD_B_MUX(forward_b_mux_out,b_out,write_data,alu_ex_mem_out,32'bz,forward_b,1'b1);
			mux2 #(32) MUX_FOR_EXT(aluSrcB,forward_b_mux_out,ext_out,(ID_opcode_out != OP_R_TYPE),1'b1);
 			alu ALU(alu_out,zero,clk,ID_alu_opration_out,forward_a_mux_out,aluSrcB);
				
	// EX/MEM Wall
		register #(32) ALU_EX_MEM_REG(alu_ex_mem_out,alu_out,1'b1,rst,clk);
		register #(32) ALU_SRC_B(alu_src_b,forward_b_mux_out,1'b1,rst,clk);
		register #(5) RD_EX_MEM(rd_ex_mem_out,rd_id_ex_out,1'b1,rst,clk);
		
	// MEM Section
		assign write_data_mem = alu_src_b;
		data_memory MEMORY(read_data_mem,alu_ex_mem_out,write_data_mem,EX_mem_read_out,EX_mem_write_out,rst,clk);
		
	// MEM/WB Wall
		register #(32) MEM_OUT(read_data_mem_reg,read_data_mem,1'b1,rst,clk);
		register #(32) ALU_MEM_WB_REG(alu_mem_wb_out,alu_ex_mem_out,1'b1,rst,clk);
		register #(5) RD_MEM_WB(rd_mem_wb_out,rd_ex_mem_out,1'b1,rst,clk);
	// WB Section
		mux2 #(32) MEM_MUX_OUT(write_data,alu_mem_wb_out,read_data_mem_reg,MEM_mem_to_reg_out,1'b1);
		// mux2 #(32) JAL_WRITE_DATA_MUX(write_data,mem_mux_out,pc_plus_4,is_jal,1'b1);
		
	
	// ID/EX controller signals register
	register#(1) ID_REG_WRITE(ID_reg_write_out,reg_write,1'b1,ID_FLUSH | rst,clk);
	register#(1) ID_ALU_SRC(ID_alu_src_out,alu_src,1'b1,ID_FLUSH | rst,clk);
	register#(1) ID_MEM_READ(ID_mem_read_out,mem_read,1'b1,ID_FLUSH | rst,clk);
	register#(1) ID_MEM_WRITE(ID_mem_write_out,mem_write,1'b1,ID_FLUSH | rst,clk);
	register#(1) ID_MEM_TO_REG(ID_mem_to_reg_out,mem_to_reg,1'b1,ID_FLUSH | rst,clk);
	register#(3) ID_ALU_OPRATION(ID_alu_opration_out,alu_operation,1'b1,ID_FLUSH | rst,clk);
	register#(6) ID_OPCODE(ID_opcode_out,opcode,1'b1,ID_FLUSH | rst,clk);

	// EXE/MEM controller sinals registers
	register#(1) EX_REG_WRITE(EX_reg_write_out,ID_reg_write_out,1'b1,EX_FLUSH | rst,clk);
	register#(1) EX_MEM_READ(EX_mem_read_out,ID_mem_read_out,1'b1,EX_FLUSH | rst,clk);
	register#(1) EX_MEM_WRITE(EX_mem_write_out,ID_mem_write_out,1'b1,EX_FLUSH | rst,clk);
	register#(1) EX_MEM_TO_REG(EX_mem_to_reg_out,ID_mem_to_reg_out,1'b1,EX_FLUSH | rst,clk);
	register#(6) EX_OPCODE(EX_opcode_out,ID_opcode_out,1'b1,ID_FLUSH | rst,clk);
	
	// MEM/WB controller sinals registers
	register#(1) MEM_REG_WRITE(MEM_reg_write_out,EX_reg_write_out,1'b1,rst,clk);
	register#(1) MEM_MEM_TO_REG(MEM_mem_to_reg_out,EX_mem_to_reg_out,1'b1,rst,clk);
	
	// Datapath Out
	assign opcode = ir_out[31:26];
	assign func = ir_out[5:0];

	//Forwarding unit
	forwarding_unit FORWARDING_UNIT(forward_a,forward_b,rd_ex_mem_out,rd_mem_wb_out, rs_out,rt_out,EX_reg_write_out,MEM_reg_write_out,ID_opcode_out);
	hazard_detection_unit HAZARD_DETECTION_UNIT(IF_FLUSH,hazard_detection_pc_write_enable,equal,opcode);
endmodule
