
module alu(output [31:0] out,output zero,input clk,input[2:0] alu_operation,input[31:0] in1,in2);
	reg[31:0] hi_,lo_;
	always@ (clk) begin
		if(alu_operation == 3'b101)
			{hi_,lo_} <=  (in1 * in2) ;
	end
	assign out = (alu_operation == 3'b000) ? in1 & in2 : 
	(alu_operation == 3'b001) ? in1 | in2 : 
	(alu_operation == 3'b010) ? in1 + in2 : 
	(alu_operation == 3'b011) ?  in1 - in2 : 
	(alu_operation == 3'b100) ? in1 ^ in2 : 
	(alu_operation == 3'b110) ? hi_ :
	(alu_operation == 3'b111) ? lo_ : 32'b0;
	assign zero = ~(|{out}); 
endmodule
