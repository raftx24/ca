
module decoder #(parameter SIZE = 4) (output [2**SIZE-1:0]out,input [SIZE-1:0]in,input enable);
	assign out = (enable) ? (1 << in) : '0 ;
endmodule
