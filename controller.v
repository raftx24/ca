
module controller(output reg reg_dst,reg_write,is_lui,alu_src,is_jal,mem_read,mem_write,mem_to_reg,
					output reg[1:0]  sel_extension,pc_select,input[5:0] opcode,input[5:0] func,input zero,rst,clk);
	// Function Field (From Instruction)
	parameter FUNC_AND = 6'b100100;
	parameter FUNC_OR = 6'b100101;
	parameter FUNC_ADD = 6'b100000;
	parameter FUNC_SUB = 6'b100010;
	parameter FUNC_XOR = 6'b100110;
	parameter FUNC_MULT = 6'b011000;
	parameter FUNC_MFLO = 6'b010010; 
	parameter FUNC_MFHI = 6'b010000;
	parameter FUNC_JR = 6'b001000;
	// Opcode Field (From Instruction)
	parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	parameter OP_J = 6'b000010;
	parameter OP_JAL = 6'b000011;
	// Extension
	parameter SIGN_EXT = 2'b00;
	parameter ZERO_EXT = 2'b01;
	parameter LUI_EXT = 2'b10;
	// PC Selection
	parameter PC_DEFAULT = 2'b00;
	parameter PC_JR = 2'b01;
	parameter PC_BRANCH = 2'b10;
	parameter PC_JUMP = 2'b11;
	
	always@(opcode, func,zero,rst,clk) begin
		reg_dst = 0;reg_write = 0;is_lui = 0;alu_src = 0;is_jal = 0;mem_read = 0;mem_write = 0;mem_to_reg = 0;
		sel_extension = SIGN_EXT;pc_select = PC_DEFAULT;
		case(opcode)
			OP_R_TYPE: begin
				if(func != FUNC_JR) begin
					reg_write = 1;reg_dst = 1;
				end else
					pc_select = PC_JR;
			end
			OP_ADDI: begin
				reg_write = 1; sel_extension = SIGN_EXT; alu_src = 1;
			end
			OP_ANDI: begin
				reg_write = 1; sel_extension = ZERO_EXT; alu_src = 1;
			end
			OP_ORI: begin
				reg_write = 1; sel_extension = ZERO_EXT; alu_src = 1;
			end
			OP_XORI: begin
				reg_write = 1; sel_extension = ZERO_EXT; alu_src = 1;
			end
			OP_LUI: begin
				reg_write = 1; sel_extension = LUI_EXT; alu_src = 1;
			end
			OP_BEQ: begin
				sel_extension = SIGN_EXT;
				if(zero == 1)
					pc_select = PC_BRANCH;
			end
			OP_BNE: begin
				sel_extension = SIGN_EXT;
				if(zero == 0)
					pc_select = PC_BRANCH;
			end
			OP_LW: begin
				sel_extension = SIGN_EXT;
				reg_dst = 0;
				alu_src = 1;
				mem_read = 1;
				reg_write=1;
				mem_to_reg=1;
			end
			OP_SW: begin
				sel_extension = SIGN_EXT;
				reg_dst = 1;
				alu_src = 1;
				mem_write = 1;
				mem_to_reg = 1;
			end
			OP_J: begin
				pc_select = PC_JUMP;
			end
			OP_JAL: begin
				is_jal = 1;
				pc_select = PC_JUMP;
			end
		endcase
	end				
endmodule