// SIZE * (WIDTH) => 1 * (WIDTH)
module mux #(parameter SIZE = 32,parameter WIDTH = 32,parameter SEL_SIZE = 5) 
	(output [WIDTH-1:0] out,input[SIZE*WIDTH-1:0] in,input [SEL_SIZE-1:0]sel,input enable);
	wire[SIZE-1:0] decoded;
	decoder #(SEL_SIZE) DECODER(decoded,sel,1'b1);
	genvar i,j;
	wire[SIZE*WIDTH-1:0] anded;
	generate
		for(i = 0; i < SIZE; i = i + 1) begin
			for(j = 0; j < WIDTH; j = j + 1) begin
				assign anded[i*WIDTH + j] = decoded[i] & in[i*WIDTH + j];
			end
		end
	endgenerate
	genvar k,l;
	generate
		for(l = 0; l < WIDTH; l = l + 1) begin
			wire[SIZE-1:0] temps;
			assign temps[0] = anded[l];
			for(k = 1; k < SIZE; k = k + 1) begin
				assign temps[k] = temps[k-1] | anded[k * WIDTH + l]; 
			end
			assign out[l] = (enable == 1'b1) ? temps[SIZE-1] : 1'bz;
		end
	endgenerate
	
endmodule
