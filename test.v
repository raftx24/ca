
module test();
	reg rst,clk = 0;
	mips MIPS(clk,rst);
	initial begin
		rst = 1;
		#220 rst = 0;
	end
	initial begin
		repeat (1000)
			#50 clk = ~clk;
	end	
endmodule
