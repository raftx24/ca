
module forwarding_unit(output reg[1:0] forward_a,forward_b,
	input[4:0] ex_mem_rd,mem_wb_rd,id_ex_rs,id_ex_rt,
						input ex_mem_reg_write,mem_wb_reg_write,input[5:0]opcode);
	reg ex_rs_hazard,ex_rt_hazard;
	parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	parameter OP_J = 6'b000010;
	parameter OP_JAL = 6'b000011;
	always@(ex_mem_rd,id_ex_rs,id_ex_rt,ex_mem_reg_write,mem_wb_reg_write,opcode) begin
		forward_a = 2'b00;
		forward_b = 2'b00;

	
		// EX Hazard
		if((ex_mem_reg_write == 1)
			&& (ex_mem_rd != 0)
				&& (ex_mem_rd == id_ex_rs))
				forward_a = 2'b10;
		if((ex_mem_reg_write == 1)
			&& (ex_mem_rd != 0)
				&& (ex_mem_rd == id_ex_rt) && ((opcode == OP_R_TYPE) | (opcode == OP_SW) | 1'b1 ))begin
					forward_b = 2'b10;
				end
		// MEM Hazard	
		if((mem_wb_reg_write == 1)
			&& (mem_wb_rd != 0)
				&& (mem_wb_rd == id_ex_rs) && (forward_a != 2'b10))
				forward_a = 2'b01;
		if((mem_wb_reg_write == 1)
			&& (mem_wb_rd != 0)
				&& (mem_wb_rd == id_ex_rt) && (forward_b != 2'b10) && ((opcode == OP_R_TYPE) |  (opcode == OP_SW) | 1'b1))
				forward_b = 2'b01;

	end
endmodule