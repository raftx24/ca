module hazard_detection_unit(output reg IF_FLUSH, pc_write_enable,input is_equal,input[5:0] opcode);
parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	parameter OP_J = 6'b000010;
	parameter OP_JAL = 6'b000011;
	always @(is_equal,opcode) begin
		pc_write_enable = 1;
		IF_FLUSH = 0;
		if((opcode == OP_BEQ && is_equal)|(opcode == OP_BNE && !is_equal) 
			| (opcode == OP_LW) | (opcode == OP_J) |(opcode == OP_JAL) )
			IF_FLUSH  = 1;
		if((opcode == OP_LW) | (opcode == OP_J) |(opcode == OP_JAL))
			pc_write_enable = 0;
	end

endmodule