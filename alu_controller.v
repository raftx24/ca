
module alu_controller(output reg[2:0] alu_operation,input[5:0] opcode,input[5:0] func);
	// Function Field (From Instruction)
	parameter FUNC_AND = 6'b100100;
	parameter FUNC_OR = 6'b100101;
	parameter FUNC_ADD = 6'b100000;
	parameter FUNC_SUB = 6'b100010;
	parameter FUNC_XOR = 6'b100110;
	parameter FUNC_MULT = 6'b011000;
	parameter FUNC_MFLO = 6'b010010; 
	parameter FUNC_MFHI = 6'b010000;
	parameter FUNC_JR = 6'b001000;
	parameter NO_OP = 6'b000000;
	// Opcode Field (From Instruction)
	parameter OP_R_TYPE = 6'b000000;
	parameter OP_ADDI = 6'b001000;
	parameter OP_ANDI = 6'b001100;
	parameter OP_ORI = 6'b001101;
	parameter OP_XORI = 6'b001110;
	parameter OP_LUI = 6'b001111;
	parameter OP_BEQ = 6'b000100;
	parameter OP_BNE = 6'b000101;
	parameter OP_LW = 6'b100011;
	parameter OP_SW = 6'b101011;
	// ALU Operations
	parameter AND = 3'b000;
	parameter OR = 3'b001;
	parameter ADD = 3'b010;
	parameter SUB = 3'b011;
	parameter XOR = 3'b100;
	parameter MULT = 3'b101;
	parameter MFHI = 3'b110;
	parameter MFLO = 3'b111;
	// Combinational Part
	always@(opcode,func) begin
		if(opcode == OP_R_TYPE) begin
			case(func)
				FUNC_AND: alu_operation = AND;
				FUNC_OR: alu_operation = OR;
				FUNC_ADD: alu_operation = ADD;
				FUNC_SUB: alu_operation = SUB;
				FUNC_XOR: alu_operation = XOR;
				FUNC_MULT: alu_operation = MULT;
				FUNC_MFLO: alu_operation = MFLO;
				FUNC_MFHI: alu_operation = MFHI;
				FUNC_JR: alu_operation = ADD;
				NO_OP : alu_operation = ADD;
			endcase
		end
		else if(opcode == OP_ANDI)
			alu_operation = AND;
		else if(opcode == OP_ORI)
			alu_operation = OR;
		else if(opcode == OP_ADDI)
			alu_operation = ADD;
		else if(opcode == OP_XORI)
			alu_operation = XOR;
		else if(opcode == OP_LUI)
			alu_operation = ADD;
		else if(opcode == OP_LW || opcode == OP_SW)
			alu_operation = ADD;
		else if(opcode == OP_BEQ || opcode == OP_BNE)
			alu_operation = SUB;
		else
			alu_operation = ADD;
	end
endmodule