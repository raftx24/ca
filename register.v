
module register #(parameter SIZE = 8) (output reg [SIZE-1:0]q,input [SIZE-1:0]d,input ld,rst,clk);
	always @(posedge clk) begin
		if(rst)	q <= '0;
		else if(ld)	q <= d;
	end
endmodule
