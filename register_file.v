
module register_file (
	output [31:0] read_data1,read_data2,
	input [4:0]read_reg1,read_reg2,write_reg,
	input[31:0] write_data,
	input write_enable,
	input clk,rst
);

	//wire[32*32-1:0] regs_out;
	//wire[31:0] ld;
	reg[31:0]mem0;
	reg[31:0]mem1;
	reg[31:0]mem2;
	reg[31:0]mem3;
	reg[31:0]mem4;
	reg[31:0]mem5;
	reg[31:0]mem6;
	reg[31:0]mem7;
	reg[31:0]mem8;
	reg[31:0]mem9;
	reg[31:0]mem10;
	reg[31:0]mem11;
	reg[31:0]mem12;
	reg[31:0]mem13;
	reg[31:0]mem14;
	reg[31:0]mem15;
	reg[31:0]mem16;
	reg[31:0]mem17;
	reg[31:0]mem18;
	reg[31:0]mem19;
	reg[31:0]mem20;
	reg[31:0]mem21;
	reg[31:0]mem22;
	reg[31:0]mem23;
	reg[31:0]mem24;
	reg[31:0]mem25;
	reg[31:0]mem26;
	reg[31:0]mem27;
	reg[31:0]mem28;
	reg[31:0]mem29;
	reg[31:0]mem30;
	reg[31:0]mem31;

	always @(posedge clk) begin
		if(rst) begin
			mem0 =0;
			mem1 = 0;
			mem2 = 0;
			mem3 = 0;
			mem4 = 0;
			mem5 = 0;
			mem6 = 0;
			mem7 = 0;
			mem8 = 0;
			mem9 = 0;
			mem10 = 0;
			mem11 = 0;
			mem12 = 0;
			mem13 = 0;
			mem14 = 0;
			mem15 = 0;
			mem16 = 0;
			mem17 = 0;
			mem18 = 0;
			mem19 = 0;
			mem20 = 0;
			mem21 = 0;
			mem22 = 0;
			mem23 = 0;
			mem24 = 0;
			mem25 = 0;
			mem26 = 0;
			mem27 = 0;
			mem28 = 0;
			mem29 = 0;
			mem30 = 0;
			mem31 = 0;
		end
		mem1 <= (write_enable & write_reg == 1) ? write_data : mem1;
		mem2 <=(write_enable & write_reg == 2) ? write_data : mem2;
		mem3 <= (write_enable & write_reg == 3) ? write_data : mem3;
		mem4 <= (write_enable & write_reg == 4) ? write_data : mem4;
		mem5 <= (write_enable & write_reg == 5) ? write_data : mem5;
		mem6 <= (write_enable & write_reg == 6) ? write_data : mem6;
		mem7 <= (write_enable & write_reg == 7) ? write_data : mem7;
		mem8 <= (write_enable & write_reg == 8) ? write_data : mem8;
		mem9 <= (write_enable & write_reg == 9) ? write_data : mem9;
		mem10 = (write_enable & write_reg == 10) ? write_data : mem10;
		mem11 = (write_enable & write_reg == 11) ? write_data : mem11;
		mem12 = (write_enable & write_reg == 12) ? write_data : mem12;
		mem13 = (write_enable & write_reg == 13) ? write_data : mem13;
		mem14 = (write_enable & write_reg == 14) ? write_data : mem14;
		mem15 = (write_enable & write_reg == 15) ? write_data : mem15;
		mem16 = (write_enable & write_reg == 16) ? write_data : mem16;
		mem17 = (write_enable & write_reg == 17) ? write_data : mem17;
		mem18 = (write_enable & write_reg == 18) ? write_data : mem18;
		mem19 = (write_enable & write_reg == 19) ? write_data : mem19;
		mem20 = (write_enable & write_reg == 20) ? write_data : mem20;
		mem21 = (write_enable & write_reg == 21) ? write_data : mem21;
		mem22 = (write_enable & write_reg == 22) ? write_data : mem22;
		mem23 = (write_enable & write_reg == 23) ? write_data : mem23;
		mem24 = (write_enable & write_reg == 24) ? write_data : mem24;
		mem25 = (write_enable & write_reg == 25) ? write_data : mem25;
		mem26 = (write_enable & write_reg == 26) ? write_data : mem26;
		mem27 = (write_enable & write_reg == 27) ? write_data : mem27;
		mem28 = (write_enable & write_reg == 28) ? write_data : mem28;
		mem29 = (write_enable & write_reg == 29) ? write_data : mem29;
		mem30 = (write_enable & write_reg == 30) ? write_data : mem30;
		mem31 = (write_enable & write_reg == 31) ? write_data : mem31;
	end

	assign read_data1 = (write_enable == 1 && write_reg == read_reg1) ? write_data : 
	(read_reg1 == 0) ? mem0 :
		(read_reg1 == 1) ? mem1 :
		(read_reg1 == 2) ? mem2 :
		(read_reg1 == 3) ? mem3 :
		(read_reg1 == 4) ? mem4 :
		(read_reg1 == 5) ? mem5 :
		(read_reg1 == 6) ? mem6 :
		(read_reg1 == 7) ? mem7 :
		(read_reg1 == 8) ? mem8 :
		(read_reg1 == 9) ? mem9 :
		(read_reg1 == 10) ? mem10 :
		(read_reg1 == 11) ? mem11 :
		(read_reg1 == 12) ? mem12 :
		(read_reg1 == 13) ? mem13 :
		(read_reg1 == 14) ? mem14 :
		(read_reg1 == 15) ? mem15 :
		(read_reg1 == 16) ? mem16 :
		(read_reg1 == 17) ? mem17 :
		(read_reg1 == 18) ? mem18 :
		(read_reg1 == 19) ? mem19 :
		(read_reg1 == 20) ? mem20 :
		(read_reg1 == 21) ? mem21 :
		(read_reg1 == 22) ? mem22 :
		(read_reg1 == 23) ? mem23 :
		(read_reg1 == 24) ? mem24 :
		(read_reg1 == 25) ? mem25 :
		(read_reg1 == 26) ? mem26 :
		(read_reg1 == 27) ? mem27 :
		(read_reg1 == 28) ? mem28 :
		(read_reg1 == 29) ? mem29 :
		(read_reg1 == 30) ? mem30 : mem31;



	assign read_data2 = (write_enable == 1 && write_reg == read_reg2) ? write_data :
		(read_reg2 == 0) ? mem0 :
		(read_reg2 == 1) ? mem1 :
		(read_reg2 == 2) ? mem2 :
		(read_reg2 == 3) ? mem3 :
		(read_reg2 == 4) ? mem4 :
		(read_reg2 == 5) ? mem5 :
		(read_reg2 == 6) ? mem6 :
		(read_reg2 == 7) ? mem7 :
		(read_reg2 == 8) ? mem8 :
		(read_reg2 == 9) ? mem9 :
		(read_reg2 == 10) ? mem10 :
		(read_reg2 == 11) ? mem11 :
		(read_reg2 == 12) ? mem12 :
		(read_reg2 == 13) ? mem13 :
		(read_reg2 == 14) ? mem14 :
		(read_reg2 == 15) ? mem15 :
		(read_reg2 == 16) ? mem16 :
		(read_reg2 == 17) ? mem17 :
		(read_reg2 == 18) ? mem18 :
		(read_reg2 == 19) ? mem19 :
		(read_reg2 == 20) ? mem20 :
		(read_reg2 == 21) ? mem21 :
		(read_reg2 == 22) ? mem22 :
		(read_reg2 == 23) ? mem23 :
		(read_reg2 == 24) ? mem24 :
		(read_reg2 == 25) ? mem25 :
		(read_reg2 == 26) ? mem26 :
		(read_reg2 == 27) ? mem27 :
		(read_reg2 == 28) ? mem28 :
		(read_reg2 == 29) ? mem29 :
		(read_reg2 == 30) ? mem30 : mem31 ;
	// genvar i;
	// register #(32) REGISTER_0(regs_out[31:0],32'b0,1'b1,rst,clk);
	// generate
	// 	for(i = 1; i < 32; i = i + 1) begin
	// 		register #(32) REGISTER_i(regs_out[(i+1)*32-1:i*32],write_data,ld[i],rst,clk);
	// 	end
	// endgenerate

	// mux #(32,32,5) MUX1(read_data1,regs_out,read_reg1,1'b1);
	// mux #(32,32,5) MUX2(read_data2,regs_out,read_reg2,1'b1);
	// decoder #(5) DECODER(ld,write_reg,write_enable);
endmodule

module register_file_test();

	wire[31:0] read_data1,read_data2;
	reg [4:0]read_reg1 = 5'b10101,read_reg2 = 5'b01010,write_reg;
	reg[31:0] write_data;
	reg write_enable = 0;
	reg clk = 0,rst = 0;
	register_file REG_FILE(read_data1,read_data2,read_reg1,read_reg2,write_reg,write_data,write_enable,clk,rst);
	initial begin
		#200 write_enable = 0;
		#200 rst = 1;
		#200 rst = 0;
		#200 write_reg = 5'b00000;
		#200 write_data = 32'b10101010101010101010101010101010;
		#200 write_enable = 1;
		#200 write_enable = 0;
		#200 read_reg1 = 5'b00000;
		#200 $stop;
	end
	initial begin
		repeat (50)
			#50 clk = ~clk;
	end
endmodule
